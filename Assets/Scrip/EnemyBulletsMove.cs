using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EnemyBulletsMove : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float speed;


    // Update is called once per frame
    private void Update()
    {
        transform.position -= new Vector3(0, speed, 0) * Time.deltaTime;
    }

}
