using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaysLoad : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject waysPoint;
    private float m_Time;
    public float m_Timedelay;
    // Update is called once per frame
    private void Awake()
    {
        m_Time = 2f;
    }
    void Update()
    {
        m_Timedelay -= Time.deltaTime;
        if(m_Timedelay   < 0)
        {
            GameObject waysPoinFake = Instantiate(waysPoint, new Vector3(0, 0, 0), Quaternion.identity);
            m_Time = m_Timedelay;

        }
    }
}
