﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization.Json;

public class Player : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float paddingLeft;
    [SerializeField] float paddingRight;
    [SerializeField] float paddingTop;
    [SerializeField] float paddingBottom;
    [SerializeField] Vector3 worldPossition;
    [SerializeField] int hp;

    [SerializeField] GameObject pannelStart;
    [SerializeField] GameObject pannelLvHard;



    public int Score = 0;
    public Text scoreText;

    public float rawInput;
    Vector2 minBounds;
    Vector2 maxBounds;


    private void Start()
    {
        Time.timeScale = 0;
      
        Score = 0;
        scoreText.text = Score.ToString();
        InitBounds();
        pannelLvHard.gameObject.SetActive(false);
       
    }
   void Update()
    {
        Move();
        Debug.Log("aaaaaa");
        if (Input.GetMouseButton(0))
        {

           if( Time.timeScale ==  1)
            {
                this.worldPossition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                this.worldPossition.z = 0;
                Vector3 newPos = Vector3.Lerp(transform.position, this.worldPossition, this.moveSpeed);
                transform.position = newPos;

                
            }
        
       
            


        }


    }
    private void InitBounds()
    {
        Camera mainCamera = Camera.main;
        minBounds = mainCamera.ViewportToWorldPoint(new Vector2(0, 0));
        maxBounds = mainCamera.ViewportToWorldPoint(new Vector2(1, 1));
    }
    private void Move()
    {




        Vector2 newPos = new Vector2();
        newPos.x = Mathf.Clamp(transform.position.x, minBounds.x + paddingLeft, maxBounds.x - paddingRight);
        newPos.y = Mathf.Clamp(transform.position.y, minBounds.y + paddingBottom, maxBounds.y - paddingTop);

        transform.position = newPos;
    }

    void FixedUpdate()
    {
       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {


        Debug.Log("aaaaaaa");
       
        scoreText.text = Score.ToString();
        if (gameObject == null)
        {
            Score = Score - 1;
            scoreText.text = Score.ToString();
           
        }
        



    }
    public void ResetScreen()
    {
        SceneManager.LoadScene(0);
    }
    public void startGame()
    {
        if(Time.timeScale == 0)
        Time.timeScale = 1;
        pannelStart.gameObject.SetActive(false);
    }
    public void NewGame()
    {
        pannelLvHard.gameObject.SetActive(true);
        pannelStart.gameObject.SetActive(false);
        SceneManager.LoadScene(0);
    }
    public void NewGame2()
    {
        SceneManager.LoadScene(0);
    }
}

