using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class LazerMove : MonoBehaviour
{
    public Player player1;
    public GameObject player2;
   
    // Start is called before the first frame update
  
    [SerializeField] private float speed;


    // Update is called once per frame
    private void Update()
    {
        transform.position += new Vector3(0, speed, 0) * Time.deltaTime;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        player1.Score++;
        if (collision.tag == "enemy")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject, 0f);
        }

    }
}
