using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn1 : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] enemysPrefab;
    int randomSpawnPoint;
    int randomEnemies;
   float time;
    public float timelate;
    void Start()
    {
        InvokeRepeating("spawnEnemy", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
       
      
    }
   
   void spawnEnemy()
    {
        randomSpawnPoint = Random.Range(0, spawnPoints.Length);
        randomEnemies = Random.Range(0, enemysPrefab.Length);
       
       
        Instantiate(enemysPrefab[randomEnemies], spawnPoints[randomSpawnPoint].position, Quaternion.identity);
            
        
    }
}

