using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove2 : MonoBehaviour
{
    Rigidbody2D rb;
    GameObject targets;
     float moveSpeed;
    Vector3 diractionToTarget;
    void Start()
    {
        targets = GameObject.Find("Player");
        rb = GetComponent<Rigidbody2D>();
        moveSpeed = Random.Range(0.1f, 3f);
    }
    // Update is called once per frame
    void Update()
    {
        enemyMove();
    }
    void enemyMove()
    {

        if (targets != null)
        {
            diractionToTarget = (targets.transform.position - transform.position).normalized;
            rb.velocity = new Vector2 (diractionToTarget.x * moveSpeed, diractionToTarget.y * moveSpeed);
        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }
}
