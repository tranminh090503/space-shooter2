using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hp : MonoBehaviour
{
    //public EnemySpawn1 EnemySpawn1;
    [SerializeField] int hp;
    public Player player;
    [SerializeField] GameObject pannelGameOver;
    void Start()
    {
        pannelGameOver.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject == null)
        {
            Time.timeScale = 0;
           
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        hp--;
        Destroy(collision.gameObject);
        if (hp < 0)
        {
            Player player1 = player.GetComponent<Player>();
            Destroy(player1.gameObject);
            Destroy(gameObject);
            Time.timeScale = 0;
            pannelGameOver.gameObject.SetActive(true);

        }
        
    }
}
