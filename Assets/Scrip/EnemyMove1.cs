using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class EnemyMove1 : MonoBehaviour
{
    public EnemyMove1 Move1;
    [SerializeField] float speedX;
    [SerializeField] float speedY;
    public GameObject target1;
    private Vector2 m_enemyPosition;
    private int m_currentTargetIndex;
    private GameObject m_currentTarget;
    private bool m_startMove;

    public float speed;
    public GameObject[] targets;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMove();
    }
        private void EnemyMove()
        {
        m_enemyPosition = new Vector2(m_enemyPosition.x, m_enemyPosition.y - speed * Time.deltaTime);
        if (m_startMove == true)
        {
            m_currentTarget = targets[m_currentTargetIndex];
            if (Vector2.Distance(transform.position, m_currentTarget.transform.position) < 0.1f)
            {
                m_currentTargetIndex++;
                if (m_currentTargetIndex > targets.Length - 1)
                {
                    m_currentTargetIndex = 0;
                }
                m_currentTarget = targets[m_currentTargetIndex];
            }
            m_enemyPosition = Vector2.MoveTowards(transform.position, m_currentTarget.transform.position, speed * Time.deltaTime);
        }
        transform.position = m_enemyPosition;
    }

}
